/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipweb.validator;

import com.exevan.ipdomain.domain.Game;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Exevan
 */
public class GameValidator implements Validator {

    @Override
    public boolean supports(Class c) {
        return Game.class.equals(c);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Game game = (Game) o;
        
        //validate title
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "game.error.title.empty");
        
        //validate genre
        ValidationUtils.rejectIfEmpty(errors, "genre", "game.error.genre.empty");
        
        //validate release date
        ValidationUtils.rejectIfEmpty(errors, "releaseDate", "game.error.releaseDate.empty");
        
        //validate publisher
        ValidationUtils.rejectIfEmpty(errors, "publisher", "game.error.publisher.empty");
        
    }
    
}
