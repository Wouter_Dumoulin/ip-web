/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipweb.validator;

import com.exevan.ipdomain.domain.Publisher;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Exevan
 */
public class PublisherValidator implements Validator {

    @Override
    public boolean supports(Class c) {
        return Publisher.class.equals(c);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Publisher publisher = (Publisher) o;
        
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "publisher.error.name.empty");
    }
    
}
