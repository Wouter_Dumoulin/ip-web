/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipweb.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Exevan
 */
public class StringToDateConverter implements Converter<String, Date> {

    @Override
    public Date convert(String dateString) {
        try {           
            //Firefox date format
            if (dateString.matches("\\d{2}-\\d{2}-\\d{4}")) {
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                return df.parse(dateString);
            //Chrome date format
            } else if (dateString.matches("\\d{4}-\\d{2}-\\d{2}")) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                return df.parse(dateString);
            } else if(dateString == null || dateString.isEmpty() || dateString.matches("\\s*"))
                return null;
            else
                throw new DateTimeException("date format incorect, should be dd-mm-yyy or yyyy-mm-dd");
        } catch (ParseException ex) {
            Logger.getLogger(StringToDateConverter.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
