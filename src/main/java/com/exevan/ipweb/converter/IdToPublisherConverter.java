/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipweb.converter;

import com.exevan.ipdomain.domain.Publisher;
import com.exevan.ipdomain.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author Exevan
 */
public class IdToPublisherConverter implements Converter<String, Publisher> {

    @Autowired
    MainService service;

    @Override
    public Publisher convert(String idString) {
        try {
            return service.getPublisher(Long.parseLong(idString));
        } catch (NumberFormatException e) {
            return null;
        }
        //throw new IllegalArgumentException("Publisher id should be a number");
    }

}
