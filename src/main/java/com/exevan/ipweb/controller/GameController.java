/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipweb.controller;

import com.exevan.ipdomain.domain.Game;
import com.exevan.ipdomain.service.MainService;
import com.exevan.ipweb.validator.GameValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Exevan
 */
//Define this class as a spring controller
@Controller
//Map this controller to the right path
@RequestMapping(value = "/games")
public class GameController {

    //Use dependency injection to instantiate the service according to dependcy inversion principle
    @Autowired
    //Interface to access domain according to dependcy inversion principle
    private MainService service;

    private final GameValidator validator;

    public GameController() {
        this.validator = new GameValidator();
    }

    //Specify that this method should only be called for GET requests
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getGames() {

        //Returns a ModelAndView object that specifies the view, model element name (used to access model element in the view) and model element
        return new ModelAndView("game/games", "games", service.getAllGames());
    }

    //Specify that this method should be called for GET requests that got to /game/add
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView getAddForm() {
        ModelAndView model = new ModelAndView("/game/addgame");
        model.addObject("game", new Game());
        model.addObject("genres", service.getAllGenres());
        model.addObject("publishers", service.getAllPublishers());
        return model;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addGame(@ModelAttribute("game") Game game, BindingResult bResult) {
        ModelAndView model = new ModelAndView("redirect:/games.htm");
        validator.validate(game, bResult);
        if (bResult.hasErrors()) {
            model.addObject("game", game);
            model.addObject("genres", service.getAllGenres());
            model.addObject("publishers", service.getAllPublishers());
            model.setViewName("/game/addgame");
        } else {
            service.addGame(game);
        }
        return model;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView getEditForm(@PathVariable long id) {
        ModelAndView model = new ModelAndView("/game/editgame");
        model.addObject("game", service.getGame(id));
        model.addObject("genres", service.getAllGenres());
        model.addObject("publishers", service.getAllPublishers());
        return model;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView editGame(@ModelAttribute("game") Game game, BindingResult bResult) {
        ModelAndView model = new ModelAndView("redirect:/games.htm");
        validator.validate(game, bResult);
        if (bResult.hasErrors()) {
            model.addObject("game", game);
            model.addObject("genres", service.getAllGenres());
            model.addObject("publishers", service.getAllPublishers());
            model.setViewName("/game/editgame");
        } else {
            service.updateGame(game.getId(), game.getTitle(), game.getGenre(), game.getReleaseDate(), game.getPublisher().getId()); 
        }
        return model;
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.GET)
    public ModelAndView getDelForm(@PathVariable long id) {
        ModelAndView model = new ModelAndView("/game/delgame");
        model.addObject("game", service.getGame(id));
        return model;
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.POST)
    public String delGame(@PathVariable long id) {
        service.removeGame(id);
        return "redirect:/games.htm";
    }
}
