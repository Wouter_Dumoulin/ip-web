/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exevan.ipweb.controller;

import com.exevan.ipdomain.domain.Publisher;
import com.exevan.ipdomain.service.MainService;
import com.exevan.ipweb.validator.PublisherValidator;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Exevan
 */
//Define this class as a spring controller
@Controller
//Map this controller to the right url
@RequestMapping(value = "/publishers")
public class PublisherController {

    //Use dependency injection to instantiate the service according to dependcy inversion principle
    @Autowired
    //Interface to access domain according to dependcy inversion principle
    private MainService service;

    private final PublisherValidator validator;

    public PublisherController() {
        this.validator = new PublisherValidator();
    }

    //Specify that this method should only be called from GET requests
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getPublishers() {

        //Returns a ModelAndView object that specifies the view, model element name (used to access model element in the view) and model element
        return new ModelAndView("publisher/publishers", "publishers", service.getAllPublishers());
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView getAddForm() {
        return new ModelAndView("/publisher/addpublisher", "publisher", new Publisher());
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addPublisher(@ModelAttribute("publisher") Publisher publisher, BindingResult bResult, SessionStatus status) {
        ModelAndView model = new ModelAndView("redirect:/publishers.htm");
        validator.validate(publisher, bResult);
        if (bResult.hasErrors()) {
            model.addObject("publisher", publisher);
            model.setViewName("publisher/addpublisher");
        } else {
            service.addPublisher(publisher);
        }
        return model;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView getEditForm(@PathVariable long id) {
        ModelAndView model = new ModelAndView("/publisher/editpublisher");
        model.addObject("publisher", service.getPublisher(id));
        return model;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView editPublisher(@ModelAttribute("publisher") Publisher publisher, BindingResult bResult, SessionStatus status) {
        ModelAndView model = new ModelAndView("redirect:/publishers.htm");
        validator.validate(publisher, bResult);
        if (bResult.hasErrors()) {
            model.addObject("publisher", publisher);
            model.setViewName("publisher/editpublisher");
        } else {
            service.updatePublisher(publisher.getId(), publisher.getName());
        }
        return model;
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.GET)
    public ModelAndView getDelForm(@PathVariable long id) {
        ModelAndView model = new ModelAndView("/publisher/delpublisher");
        model.addObject("publisher", service.getPublisher(id));
        return model;
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.POST)
    public String delGame(@PathVariable long id) {
        service.removePublisher(id);
        return "redirect:/publishers.htm";
    }
}
