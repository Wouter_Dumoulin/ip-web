<%-- 
    Document   : games
    Created on : Apr 2, 2016, 3:49:31 AM
    Author     : Exevan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    
    <jsp:include page="/WEB-INF/jsp/head.jsp">
        <jsp:param name="title" value="Find your favourite games here" />
    </jsp:include>

    <body>
        <%@include file="/WEB-INF/jsp/header.jsp" %>
        <div class="container">
            <div id="screen">
                <div id="content">
                    <div class="table-responsive">
                        <table id="overview" class="table table-striped sortable">
                            <thead>
                                <tr>
                                    <th class="sortable"><spring:message code="games.id" text="Id"/></th>
                                    <th data-defaultsort="asc" class="sortable"><spring:message code="games.title" text="Title"/></th>
                                    <th class="sortable"><spring:message code="games.genre" text="Genre"/></th>
                                    <th class="sortable"><spring:message code="games.releasedate" text="Release date"/></th>
                                    <th class="sortable"><spring:message code="games.publisher" text="Publisher"/></th>
                                    <th data-defaultsort="disabled"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="game" items="${games}">
                                    <tr>
                                        <td>${game.id}</td>
                                        <td>${game.title}</td>
                                        <td>${game.genre}</td>                                        
                                        <td data-dateformat="DD-MM-YYYY"><fmt:formatDate value="${game.releaseDate}" type="date" pattern="dd-MM-yyyy" /></td>
                                        <td>${game.publisher.name}</td>
                                        <td>
                                            <a href="<c:url value="/games/edit/${game.id}.htm"/>"><span class="glyphicon glyphicon-small glyphicon-pencil"/></a>
                                            <a href="<c:url value="/games/del/${game.id}.htm"/>"><span class="glyphicon glyphicon-small glyphicon-remove"/></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <form method="GET" action="<c:url value="/games/add.htm"/>">
                                            <input class="btn btn-primary btn-block" type="submit" value="+"/>
                                        </form>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
