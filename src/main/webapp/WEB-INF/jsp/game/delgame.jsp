<%-- 
    Document   : delgame
    Created on : Apr 12, 2016, 8:18:33 PM
    Author     : Exevan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

    <jsp:include page="/WEB-INF/jsp/head.jsp">
        <jsp:param name="title" value="Delete games here" />
    </jsp:include>

    <body>

        <%@include file="/WEB-INF/jsp/header.jsp" %>
        <div class="container">
            <div class="table-responsive">
                <h1><spring:message code="delgame.header" text="Delete games here"/></h1>
                <table id="gameOverview" class="table">
                    <tbody>
                        <tr>
                            <th><spring:message code="delgame.id" text="Id"/></th>
                            <td>${game.id}</td>
                        </tr>
                        <tr>
                            <th><spring:message code="delgame.title" text="Title"/></th>
                            <td>${game.title}</td>
                        </tr>
                        <tr>
                            <th><spring:message code="delgame.genre" text="Genre"/></th>
                            <td>${game.genre}</td>
                        </tr>
                        <tr>
                            <th><spring:message code="delgame.releasedate" text="Release date"/></th>
                            <td><fmt:formatDate value="${game.releaseDate}" type="date" pattern="dd-MM-yyyy" /></td>
                        </tr>
                        <tr>
                            <th><spring:message code="delgame.publisher" text="P"/></th>
                            <td>${game.publisher.name}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2">
                                <p style="color: red;"><spring:message code="delgame.warning" text="Warning: removing a game cannot be undone!"/></p>
                                <spring:message code="delgame.submit" var="submitmessage"/>
                                <form method="POST" action="<c:url value="/games/del/${game.id}.htm"/>">
                                    <input class="btn btn-primary btn-block" type="submit" value="${submitmessage}"/>
                                </form>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </body>
</html>
