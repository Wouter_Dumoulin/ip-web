<%-- 
    Document   : editgame
    Created on : Apr 7, 2016, 3:24:24 PM
    Author     : Exevan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <jsp:include page="/WEB-INF/jsp/head.jsp">
        <jsp:param name="title" value="Edit games here" />
    </jsp:include>

    <body>
        <%@include file="/WEB-INF/jsp/header.jsp" %>
        <div class="container">
            <h1><spring:message code="editgame.header" text="Edit games here"/></h1>

            <c:url var="post_url"  value="/games/edit.htm" />
            <form:form id="gameform" role="form" method="post" modelAttribute="game" action="${post_url}">

                <form:hidden path="id"/>

                <div class="form-group">
                    <label for="title"><spring:message code="editgame.title" text="Title"/></label>
                    <form:input path="title" type="text" class="form-control" id="title" name="title" placeholder="Title"/>
                    <form:errors path="title" element="div" cssClass="alert alert-small alert-danger"/>
                </div>

                <div class="form-group">
                    <label for="genre"><spring:message code="editgame.genre" text="Genre" var="genremessage"/>${genremessage}</label>
                    <form:select path="genre" class="form-control" id="genre" name="genre" placeholder="Genre">
                        <form:option label="${genremessage}" value="" disabled="true" selected="true"/>
                        <form:options items="${genres}"/>                   
                    </form:select>
                    <form:errors path="genre" element="div" cssClass="alert alert-small alert-danger"/>
                </div>

                <div class="form-group">
                    <label for="releaseDate"><spring:message code="editgame.releasedate" text="Release date"/></label>
                    <spring:message code="editgame.dateformat" var="dateformatmessage"/>
                    <form:input path="releaseDate" type="date" class="form-control" id="releaseDate" name="releaseDate" placeholder="${dateformatmessage}"/>
                    <form:errors path="releaseDate" element="div" cssClass="alert alert-small alert-danger"/>
                </div>

                <div class="form-group">
                    <label for="publisher"><spring:message code="editgame.publisher" text="Publisher" var="publishermessage"/>${publishermessage}</label>
                    <form:select path="publisher" class="form-control" id="publisher" name="publisher" placeholder="Publisher">
                        <form:option label="${publishermessage}" value="" disabled="true" selected="true"/>
                        <form:options items="${publishers}" itemLabel="name" itemValue="id"/>                  
                    </form:select>
                    <form:errors path="publisher" element="div" cssClass="alert alert-small alert-danger"/>
                </div>

                <spring:message code="editgame.submit" var="submitmessage"/>
                <input class="btn btn-primary btn-block" type="submit" value="${submitmessage}">
            </form:form>
        </div>
    </body>
</html>
