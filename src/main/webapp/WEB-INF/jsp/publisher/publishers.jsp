<%-- 
    Document   : publishers
    Created on : Apr 2, 2016, 3:21:07 PM
    Author     : Exevan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <jsp:include page="/WEB-INF/jsp/head.jsp">
        <jsp:param name="title" value="Find game publishers here" />
    </jsp:include>

    <body>
        <div id="container">
            <%@include file="/WEB-INF/jsp/header.jsp" %>
            <div class="container">
                <div id="screen">
                    <div id="content">
                        <div class="table-responsive">
                            <table id="overview" class="table table-striped sortable">
                                <thead>
                                    <tr>
                                        <th class="sortable"><spring:message code="publishers.id" text="Id"/></th>
                                        <th data-defaultsort="asc" class="sortable"><spring:message code="publishers.name" text="Name"/></th>
                                        <th class="sortable"><spring:message code="publishers.nbgames" text="Number of games"/></th>
                                        <th data-defaultsort="disabled"><spring:message code="publishers.games" text="Games"/></th>
                                        <th data-defaultsort="disabled"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="publisher" items="${publishers}">
                                        <tr>
                                            <td>${publisher.id}</td>
                                            <td>${publisher.name}</td>
                                            <td>${fn:length(publisher.games)}</td>
                                            <td>
                                                <c:forEach var="game" items="${publisher.games}">
                                                    <p>${game.title}</p>
                                                </c:forEach>
                                            </td>
                                            <td>
                                                <a href="<c:url value="/publishers/edit/${publisher.id}.htm"/>"><span class="glyphicon glyphicon-small glyphicon-pencil"/></a>
                                                <a href="<c:url value="/publishers/del/${publisher.id}.htm"/>"><span class="glyphicon glyphicon-small glyphicon-remove"/></a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            <form method="GET" action="<c:url value="/publishers/add.htm"/>">
                                                <input class="btn btn-primary btn-block" type="submit" value="+"/>
                                            </form>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
