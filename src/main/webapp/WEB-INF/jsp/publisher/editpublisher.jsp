<%-- 
    Document   : editpublisher
    Created on : Apr 13, 2016, 3:59:16 AM
    Author     : Exevan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html>
    <jsp:include page="/WEB-INF/jsp/head.jsp">
        <jsp:param name="title" value="Edit publishers here" />
    </jsp:include>

    <body>
        <%@include file="/WEB-INF/jsp/header.jsp" %>
        <div class="container">
            <h1><spring:message code="editpublisher.header" text="Edit publishers here"/></h1>

            <c:url var="post_url"  value="/publishers/edit.htm" />
            <form:form id="publisherform" role="form" method="post" modelAttribute="publisher" action="${post_url}">

                <form:hidden path="id"/>

                <div class="form-group">
                    <label for="Name"><spring:message code="editpublisher.name" var="namemessage"/>${namemessage}</label>                   
                    <form:input path="name" type="text" class="form-control" id="name" name="name" placeholder="${namemessage}"/>
                    <form:errors path="name" element="div" cssClass="alert alert-small alert-danger"/>
                </div>
                
                <spring:message code="editpublisher.submit" var="submitmessage"/>
                <input class="btn btn-primary btn-block" type="submit" value="${submitmessage}">
            </form:form>
        </div>
    </body>
</html>
