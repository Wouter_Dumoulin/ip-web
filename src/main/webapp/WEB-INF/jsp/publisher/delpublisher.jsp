<%-- 
    Document   : delpublisher
    Created on : Apr 13, 2016, 3:59:31 AM
    Author     : Exevan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

    <jsp:include page="/WEB-INF/jsp/head.jsp">
        <jsp:param name="title" value="Delete publishers here" />
    </jsp:include>

    <body>

        <%@include file="/WEB-INF/jsp/header.jsp" %>
        <div class="container">
            <div class="table-responsive">
                <h1><spring:message code="delpublisher.header" text="Delete publishers here"/></h1>
                <table id="publisherOverview" class="table">
                    <tbody>
                        <tr>
                            <th><spring:message code="delpublisher.id" text="Id"/></th>
                            <td>${publisher.id}</td>
                        </tr>
                        <tr>
                            <th><spring:message code="delpublisher.name" text="Name"/></th>
                            <td>${publisher.name}</td>
                        </tr>  
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2">
                                <p style="color: red;"><spring:message code="delpublisher.warning" text="Warning: removing a publisher will also remove all of its games and cannot be undone!"/></p>
                                <spring:message code="delpublisher.submit" var="submitmessage"/>
                                <form method="POST" action="<c:url value="/publishers/del/${publisher.id}.htm"/>">
                                    <input class="btn btn-primary btn-block" type="submit" value="${submitmessage}"/>
                                </form>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </body>
</html>
