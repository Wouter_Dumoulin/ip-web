<%-- 
    Document   : addpublisher
    Created on : Apr 4, 2016, 12:25:08 AM
    Author     : Exevan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <jsp:include page="/WEB-INF/jsp/head.jsp">
        <jsp:param name="title" value="Add publishers here" />
    </jsp:include>

    <body>
        <%@include file="/WEB-INF/jsp/header.jsp" %>
        <div class="container">
            <h1><spring:message code="addpublisher.header" text="Add publishers here"/></h1>

            <c:url var="post_url" value="/publishers/add.htm" />
            <form:form id="publisherform" role="form" method="post" modelAttribute="publisher" action="${post_url}">

                <form:hidden path="id"/>

                <div class="form-group">
                    <label for="name"><spring:message code="addpublisher.name" var="namemessage"/>${namemessage}</label>
                    <form:input path="name" type="text" class="form-control" id="name" name="name" placeholder="${namemessage}"/>
                    <form:errors path="name" element="div" cssClass="alert alert-small alert-danger"/>
                </div>

                <spring:message code="addpublisher.submit" var="submitmessage"/>
                <input class="btn btn-primary btn-block" type="submit" value="${submitmessage}">
            </form:form>
        </div>
    </body>
</html>
