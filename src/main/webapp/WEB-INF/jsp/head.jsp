<%-- 
    Document   : head
    Created on : Apr 2, 2016, 3:16:36 AM
    Author     : Exevan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>${param.title}</title>

        <!-- jQuery  -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        

        <!-- Bootstrap -->
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
        <!-- Bootstrap css -->
        <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        
 
        <!-- Bootstrap-sortable script for sortable html tables with bootstrap -->
        <script type="text/javascript" src="https://raw.githubusercontent.com/drvic10k/bootstrap-sortable/master/Scripts/bootstrap-sortable.js"></script>
        
        <!-- Moment script for bootstrap-sortable, used to compare dates -->
        <script type="text/javascript" src="https://raw.githubusercontent.com/drvic10k/bootstrap-sortable/master/Scripts/moment.min.js"></script>
        
        <!-- Sortable bootstrap tables stylesheet -->
        <!-- This file has to be local since the online version does not get recognized as css -->
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/bootstrap-sortable.css" />">
        
        
        <!-- Stylesheet to finetune specific styles -->
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/css-tuning.css" />">       
    </head>