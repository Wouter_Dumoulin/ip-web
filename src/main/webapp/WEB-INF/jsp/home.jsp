<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    
   
    <jsp:include page="/WEB-INF/jsp/head.jsp">
        <jsp:param name="title" value="VidyaDB" />
    </jsp:include>

    <body>
        <%@include file="/WEB-INF/jsp/header.jsp" %>
        <div class="container">
            <div id="screen">
                <div id="content">
                    <h1>VidyaDB</h1>
                    <p><spring:message code="home.welcome" text="Hello and welcome to VidyaDB, your primary database for all your games. Add, remove and edit all your favorite games and publishers"/></p>
                </div>
            </div>
        </div>
    </body>
</html>
