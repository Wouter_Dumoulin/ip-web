<%-- 
    Document   : header
    Created on : Apr 2, 2016, 3:16:28 AM
    Author     : Exevan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<header>
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
        <a href="?lang=en">English</a>
        <a href="?lang=nl">Nederlands</a>
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="<c:url value="/home.htm"/>"><spring:message code="header.home" text="Home"/></a>
                <a class="navbar-brand" href="<c:url value="/games.htm"/>"><spring:message code="header.games" text="Games"/></a>
                <a class="navbar-brand" href="<c:url value="/publishers.htm"/>"><spring:message code="header.publishers" text="Publishers"/></a>
            </div>
        </div>
    </div>
</header>